require("dotenv").config();
const express = require("express");
const app = express();
const cors = require('cors')
const { setupDB } = require("./data/setupSqliteDb");
const errors = require("./middlewares/errors");
//middlwares
app.use(express.json());
app.use(require("morgan")("dev"));
app.use(cors())
//routes
app.use("/api/services/auth", require("./routes/auth"));
app.use("/api/services/usuarios", require("./routes/users"));

//middleware error
app.use(errors);

async function initSqlite() {
  try {
    const db = await setupDB();

    if (db) {
      initServer();
    }
  } catch (error) {
    console.log(error);
  }
}

function initServer() {
  const port = process.env.EXPRESS_PORT || 8100;
  const ip = process.env.EXPRESS_SERVER_IP || "localhost";
  app.listen(port, ip, () => {
    console.log(`Servidor express esuchando en el puerto http://${ip}:${port}`);
  });
}

initSqlite();
