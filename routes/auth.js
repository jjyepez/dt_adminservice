const express = require("express");
const router = express.Router();
const basicAuth = require("../utils/auth/basic");
const { success } = require("../response");
router.post("/", basicAuth("basic"), async (req, res, next) => {
  try {
    success(res, req.payload, 200);
  } catch (error) {
    next(error);
  }
});

module.exports = router;
