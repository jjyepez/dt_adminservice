const express = require("express");
const router = express.Router();
const Services = require("../services/users");
const { success } = require("../response");
router.get("/", (req, res, next) => {
  Services.getUsers()
    .then((data) => {
      success(res, data, 200);
    })
    .catch((error) => {
      next(error);
    });
});

module.exports = router;
