const { listUsers } = require("../data/setupSqliteDb");

async function getUsers(next) {
  try {
    const users = await listUsers();
    return users || [];
  } catch (error) {
    next(error);
  }
}

module.exports = {
  getUsers,
};
