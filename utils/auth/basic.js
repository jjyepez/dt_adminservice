const handleError = require("../handleError");
const { listUser } = require("../../data/setupSqliteDb");
function basicAuth(type) {
  async function basicMiddlware(req, res, next){
    switch(type) {
      case 'basic':
      const username = req.body.username || "";
      const password = req.body.password || "";
      try {
        const user = await listUser("user", username);//columna, valor
        if (!user || user.password !== password) {
          throw handleError("No esta autorizado", 401);
        }      
        delete user.password;
        const payload = {
          token: "token-test",
          user: user
      } 
      req.payload = payload
        next()
        break;
      } catch (error) {
        next(error)
        break;
      }


      default:
      next();
    }
  }
  return basicMiddlware
}

module.exports = basicAuth;
