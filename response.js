function success(res, result, status) {
  let statusCode = status || 200;

  res.status(statusCode).send({
    error: false,
    message: "OK",
    data: result,
  });
}

function error(res, message, status) {
  res.status(status).send({
    error: true,
    message: message,
    data: {},
  });
}

module.exports = {
  success,
  error,
};
