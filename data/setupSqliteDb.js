const { Database } = require("sqlite3").verbose();
const path = require("path");
const db = new Database(path.join(__dirname, "./", "usuarios.db"));
const queries = {
  tableRoles: `
    CREATE TABLE IF NOT EXISTS roles (
        id INTEGER AUTO_INCREMENT,
        name TEXT(100),
        nivel INTEGER NOT NULL DEFAULT 10,
        permisos_denegados json,

        CONSTRAINT rol_id_pk PRIMARY KEY (id)
    );
    `,
  tableUsuariosStatus: `
    CREATE TABLE IF NOT EXISTS usuarios_status (
        id INTEGER AUTO_INCREMENT,
        name TEXT(100),

        CONSTRAINT user_states_pk PRIMARY KEY (id)
);
 `,

  tableUsers: `
  CREATE TABLE IF NOT EXISTS usuarios (
        id INTEGER AUTO_INCREMENT,
        user TEXT(20),
        password TEXT(200),
        nombres TEXT(200),
        apellidos TEXT(200),
        email TEXT(200),
        status_id INTEGER NOT NULL,
        rol_id INTEGER NOT NULL,
        info_contacto TEXT(200),

        PRIMARY KEY (id),
        FOREIGN KEY (rol_id)
            REFERENCES roles(id)
            ON DELETE RESTRICT
            ON UPDATE CASCADE

        FOREIGN KEY (status_id)
            REFERENCES usuarios_status(id)
            ON DELETE RESTRICT
            ON UPDATE CASCADE
    );`,

  tableAudits: `
    CREATE TABLE IF NOT EXISTS auditoria (
          id INTEGER AUTO_INCREMENT,
          dispositivo INTEGER,
          fecha TIMESATMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
          accion TEXT(30) DEFAULT 'CEACRION',
          descripcion TEXT(200),
          usuario INTEGER NOT NULL,
  
          PRIMARY KEY (id),
          FOREIGN KEY (usuario)
              REFERENCES usuarios(id)
              ON DELETE RESTRICT
              ON UPDATE CASCADE
      );`,
};

module.exports = {
  setupDB: () => {
    return new Promise((resolve, reject) => {
      db.serialize(() => {
        db.run(queries.tableRoles);
        db.run(queries.tableUsuariosStatus);
        db.run(queries.tableUsers);
        db.run(queries.tableAudits, (err) => {
          if (err) {
            return reject(err);
          }
          resolve(true);
        });
      });
    });
  },
  listUser: (where, value) => {
    return new Promise((resolve, reject) => {
      const select = "user, password, nombres, apellidos, email, rol_id, roles.name as rolName, nivel, permisos_denegados, usuarios_status.name as statusName"
      let stmt = db.prepare(`
      SELECT ${select} FROM usuarios 
      INNER JOIN roles ON roles.id = usuarios.rol_id
      INNER JOIN usuarios_status ON usuarios_status.id = usuarios.status_id
      where ${where} = ?`);
      stmt.get(`${value}`, (err, row) => {
        if (err) return reject(err);
        resolve(row);
        stmt.finalize();
      });
    });
  },
  listUsers: () => {
    const select = "user, nombres, apellidos, email, rol_id, roles.name as rolName, nivel, permisos_denegados, usuarios_status.name as statusName"

    return new Promise((resolve, reject) => {
      let stmt = db.prepare(`SELECT ${select} FROM usuarios
      INNER JOIN roles ON roles.id = usuarios.rol_id
      INNER JOIN usuarios_status ON usuarios_status.id = usuarios.status_id
      `);
      stmt.all((err, rows) => {
        if (err) return reject(err);
        resolve(rows);
        stmt.finalize();
      });
    });
  },
};
